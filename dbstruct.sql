CREATE DATABASE `tt`;
USE `tt`;
DROP TABLE IF EXISTS `links`;
CREATE TABLE `links` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `link` TEXT,
  `code` CHAR(6),
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


