<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use model\manager\LinksManager;
use model\LinksDataMapper;
use model\LinksCacheMapper;

final class LinksManagerTest extends TestCase
{

    /**
     * @return array
     */
    public function dataProvider(): array
    {
        return [
            [
                'aaa111',
                'http://yandex.ru',
                [
                    'id' => '1',
                    'link' => 'http://yandex.ru',
                    'code' => 'aaa111',
                ],
            ],
        ];
    }

    /**
     * @dataProvider dataProvider
     *
     * @param $code
     * @param $link
     * @param $data
     */
    public function testGetLinkFromDB($code, $link, $data)
    {
        $dataMapper = $this->createMock(LinksDataMapper::class);
        $dataMapper->method('find')->willReturn($data);
        $cacheMapper = $this->createMock(LinksCacheMapper::class);
        $cacheMapper->method('setData')->willReturn([]);
        $entityManager = new LinksManager(
            false,
            $dataMapper,
            $cacheMapper
        );

        $data = $entityManager->getData($code);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals($data['code'], $code);
        $this->assertEquals($data['link'], $link);
    }

    /**
     * @dataProvider dataProvider
     *
     * @param $code
     * @param $link
     * @param $data
     */
    public function testSetLinkToDB($code, $link, $data)
    {
        $dataMapper = $this->createMock(LinksDataMapper::class);
        $dataMapper->method('setData')->willReturn($data);
        $cacheMapper = $this->createMock(LinksCacheMapper::class);
        $entityManager = new LinksManager(
            false,
            $dataMapper,
            $cacheMapper
        );

        $data = $entityManager->setData($code, $link);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals($data['code'], $code);
        $this->assertEquals($data['link'], $link);
    }

    /**
     * @dataProvider dataProvider
     *
     * @param $code
     * @param $link
     * @param $data
     */
    public function testGetLinkFromCache($code, $link, $data)
    {
        $cacheMapper = $this->createMock(LinksCacheMapper::class);
        $cacheMapper->method('find')->willReturn($data);
        $dataMapper = $this->createMock(LinksDataMapper::class);
        $entityManager = new LinksManager(
            true,
            $dataMapper,
            $cacheMapper
        );

        $data = $entityManager->getData($code);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals($data['code'], $code);
        $this->assertEquals($data['link'], $link);
    }

}
