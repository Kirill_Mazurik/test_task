<?php

$app['custom.domain'] = 'http://192.168.33.16/gate/';

$app['custom.links.datamapper'] = \model\LinksDataMapper::class;
$app['custom.links.cachemapper'] = \model\LinksCacheMapper::class;

\component\Factory::registerService('mysql', \component\MysqlAdapter::class);
\component\Factory::registerService('redis', \component\RedisAdapter::class);