#!/usr/bin/env bash

add-apt-repository -y ppa:ondrej/php > /dev/null 2>&1
add-apt-repository ppa:chris-lea/redis-server

apt-get -qq update

apt-get -y install php7.1-fpm php7.1-mysql php7.1-xml php7.1-mbstring php7.1-zip php7.1-curl git nginx redis-server > /dev/null 2>&1

echo "mysql-server mysql-server/root_password password root" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password root" | debconf-set-selections
apt-get -y install mysql-server mysql-client > /dev/null 2>&1

rm -rf /etc/nginx/sites-enabled/default
cp /vagrant/tt.conf /etc/nginx/conf.d/
service nginx restart

curl --silent https://getcomposer.org/installer | php > /dev/null 2>&1
mv composer.phar /usr/local/bin/composer
chmod 0755 /usr/local/bin/composer

wget https://phar.phpunit.de/phpunit.phar /dev/null 2>&1
chmod +x phpunit.phar
mv phpunit.phar /usr/local/bin/phpunit

echo "run 'cd /vagrant && composer install' if need to"
