<?php

declare(strict_types=1);

namespace controller;

use component\GoogleShorten;
use model\manager\LinksManager;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Вызов сокращателя url
 *
 * Class Compressor
 * @package controller
 */
class Compressor
{

    /**
     * @param Request $request
     * @param Application $app
     * @return string
     */
    public function post(Request $request, Application $app): string
    {
        $link = '';
        $url = $request->get('url');
        if (!empty($url)) {
            $code = GoogleShorten::getShortCode($url);

            if ($code) {
                $entityManager = new LinksManager(
                    true,
                    new $app['custom.links.datamapper'],
                    new $app['custom.links.cachemapper']
                );
                $entityManager->setData($url, $code);
                $link = $app['custom.domain'] . $code;
            }
        }

        return json_encode([
            'link' => $link,
        ]);
    }

}
