<?php

declare(strict_types=1);

namespace controller;

use model\manager\LinksManager;
use Silex\Application;

/**
 * Контроллер Gate
 *
 * Class Gate
 * @package controller
 */
class Gate
{

    /**
     * @param Application $app
     * @param $code
     */
    public function get(Application $app, $code)
    {
        $entityManager = new LinksManager(
            true,
            new $app['custom.links.datamapper'],
            new $app['custom.links.cachemapper']
        );

        try {
            $data = $entityManager->getData($code);

            $url = $data['link'];
            if (empty(parse_url($url, PHP_URL_SCHEME))) {
                $url = 'http://' . $url;
            }

            header("Location: " . $url);
        } catch (\Exception $e) {
            header("Location: /");
        }

        exit;
    }

}
