<?php

declare(strict_types=1);

class Provider implements \Silex\Api\ControllerProviderInterface
{

    public function connect(\Silex\Application $app)
    {
        $factory = $app["controllers_factory"];

        $app->get("/gate/{code}", 'controller\Gate::get');
        $app->post("/compressor", 'controller\Compressor::post');

        return $factory;
    }
}