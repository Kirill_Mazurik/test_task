<?php

namespace component;

/**
 * Простой самописный контейнер для сервисов на основе паттернов singleton, serviceLocator и flyweight
 *
 * Class Factory
 * @package component
 */
class Factory
{

    private static $services = [];
    private static $registry = [];

    /**
     * @param string $serviceName
     * @return bool|mixed
     */
    public static function getInstance(string $serviceName)
    {
        if (!array_key_exists($serviceName, self::$registry)) {
            return false;
        }

        if (!array_key_exists($serviceName, self::$services)) {
            self::$services[$serviceName] = new self::$registry[$serviceName];
        }

        return self::$services[$serviceName];
    }

    /**
     * @param string $serviceName
     * @param string $className
     */
    public static function registerService(string $serviceName, string $className)
    {
        self::$registry[$serviceName] = $className;
    }

}