<?php

declare(strict_types=1);

namespace component;

use Aura\Sql\ExtendedPdo;

/**
 * Адаптер для ExtendedPdo
 *
 * Class MysqlAdapter
 * @package component
 */
class MysqlAdapter
{

    private $pdo;

    // @todo вынести в конфиг
    private $dsn = 'mysql:host=localhost;dbname=tt';
    private $username = 'root';
    private $password = 'root';


    public function __construct()
    {
        $this->pdo = new ExtendedPdo($this->dsn, $this->username, $this->password);
    }

    /**
     * @param $query
     * @param $params
     * @return array
     */
    public function fetchOne($query, $params): array
    {
        $result = $this->pdo->fetchOne($query, $params);
        if (!$result) {
            $result = [];
        }
        return $result;
    }

    /**
     * @param $query
     * @param $params
     * @return int
     */
    public function insert($query, $params): int
    {
        $result = $this->pdo->perform($query, $params);
        return (integer)$this->pdo->lastInsertId();
    }
}