<?php

declare(strict_types=1);

namespace component;

use Predis\Client;

/**
 * Адаптер для Predis
 *
 * Class RedisAdapter
 * @package component
 */
class RedisAdapter
{

    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param $key
     * @return string
     */
    public function get($key): string
    {
        $value = $this->client->get($key);
        if (empty($value)) {
            $value = '';
        }
        return $value;
    }

    /**
     * @todo сделать проверку возвращаемого значения
     *
     * @param $key
     * @param $value
     * @param int $ttl
     * @return bool
     */
    public function set($key, $value, $ttl = 0): bool
    {
        if ($ttl) {
            $result = $this->client->set($key, $value, 'ex', $ttl);
        } else {
            $result = $this->client->set($key, $value);
        }
        return true;
    }

}