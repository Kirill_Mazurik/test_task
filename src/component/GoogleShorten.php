<?php

declare(strict_types=1);

namespace component;

use \dotzero\Googl;

/**
 * Сокращатель ссылок через сервис google
 *
 * Class GoogleShorten
 * @package component
 */
class GoogleShorten
{

    const API_KEY = 'AIzaSyAhunZtn-xo108QZ9awh8_y-Y35hPU1TmQ'; //@todo move to config
    const REPLACE = 'https://goo.gl/';

    /**
     * Сокращает url и возвращает новую
     *
     * @param string $url
     * @return string
     */
    public static function getShortUrl(string $url): string
    {
        $googl = new Googl(self::API_KEY);
        $newUrl = $googl->shorten($url);
        return $newUrl;
    }

    /**
     * Сокращает url и возвращает уникальный код
     *
     * @param string $url
     * @return string
     */
    public static function getShortCode(string $url): string
    {
        $newUrl = '';
        $googl = new Googl(self::API_KEY);
        try {
            $newUrl = $googl->shorten($url);
        } catch (\Exception $e) {
            return $newUrl;
        }

        $code = str_replace(self::REPLACE, '', $newUrl);
        return substr($code, 0, 6);
    }

}