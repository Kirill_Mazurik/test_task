<?php

declare(strict_types=1);

namespace model;

/**
 * Модель таблицы links
 *
 * Class Links
 * @package model
 */
class Links implements Common
{
    private $id;
    private $link;
    private $code;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink($link)
    {
        $this->link = $link;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function hydrate($data)
    {
        $this->id = $data['id'] ?? null;
        $this->link = $data['link'] ?? null;
        $this->code = $data['code'] ?? null;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'link' => $this->link,
            'code' => $this->code,
        ];
    }
}