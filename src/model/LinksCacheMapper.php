<?php

declare(strict_types=1);

namespace model;

use component\Factory;

/**
 * DataMapper модели Links в noSQL
 *
 * Class LinksCacheMapper
 * @package model
 */
class LinksCacheMapper implements DataMapper
{
    const TTL = 3600;

    private $model;
    private $connection;

    public function __construct()
    {
        $this->model = new Links();
        $this->connection = Factory::getInstance('redis');
    }

    /**
     * Поиск url по коду
     *
     * @param $key
     * @return array
     */
    public function find($key): array
    {
        $data = $this->connection->get($key);

        if ($data) {
            $this->model->hydrate([
                'link' => $data,
                'code' => $key,
            ]);

            return $this->model->toArray();
        } else {
            return [];
        }
    }

    /**
     * @param $key
     * @param $value
     * @return array
     */
    public function setData($key, $value): array
    {
        $result = $this->connection->set($key, $value, self::TTL);
        if ($result) {
            $this->model->hydrate([
                'link' => $result,
                'code' => $key,
            ]);

            return $this->model->toArray();
        } else {
            return [];
        }
    }
}