<?php

namespace model;

interface DataMapper
{
    public function find($code): array;

    public function setData($url, $code): array;
}