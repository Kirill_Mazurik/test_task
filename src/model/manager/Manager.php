<?php

declare(strict_types=1);

namespace model\manager;

use model\DataMapper;

/**
 * EntityManager абстракция для работы с СУБД и noSQL
 *
 * Class Manager
 * @package model\manager
 */
class Manager
{

    protected $useCache = false;
    protected $dataMapper;
    protected $cacheMapper;

    /**
     * Переключатель использования кеша
     *
     * @param $useCache
     */
    public function setUseCache($useCache)
    {
        $this->useCache = $useCache;
    }

    public function setDataMapper(DataMapper $dataMapper)
    {
        $this->dataMapper = $dataMapper;
    }

    public function setCacheMapper(DataMapper $cacheMapper)
    {
        $this->cacheMapper = $cacheMapper;
    }

}