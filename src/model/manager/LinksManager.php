<?php

declare(strict_types=1);

namespace model\manager;

use model\DataMapper;

/**
 * Менеджер (модели/ентити Links), инкапсулирующий работу с БД и кэшем
 *
 * Class LinksManager
 * @package model\manager
 */
class LinksManager extends Manager
{

    public function __construct(
        bool $useCache,
        DataMapper $dataMapper,
        DataMapper $cacheMapper
    )
    {
        $this->useCache = $useCache;
        $this->dataMapper = $dataMapper;
        $this->cacheMapper = $cacheMapper;
    }

    /**
     * Извлечение данных
     *
     * @param string $code
     * @return array
     */
    public function getData(string $code): array
    {
        if ($this->useCache) {
            $data = $this->cacheMapper->find($code);
            if (!empty($data)) {
                return $data;
            }
        }

        $result = $this->dataMapper->find($code);
        $this->cacheMapper->setData($code, $result['link']);
        return $result;
    }

    /**
     * Сохранение данных
     *
     * @param string $url
     * @param string $code
     * @return array
     */
    public function setData(string $url, string $code): array
    {
        return $this->dataMapper->setData($url, $code);
    }

}