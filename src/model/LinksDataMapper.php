<?php

declare(strict_types=1);

namespace model;

use component\Factory;

/**
 * DataMapper модели Links в СУБД
 *
 * Class LinksDataMapper
 * @package model
 */
class LinksDataMapper implements DataMapper
{
    private $model;
    private $connection;

    public function __construct()
    {
        $this->model = new Links();
        $this->connection = Factory::getInstance('mysql');
    }

    /**
     * Поиск url по коду
     *
     * @param $code
     * @return array
     * @throws \Exception
     */
    public function find($code): array
    {
        $query = "SELECT id, link
                  FROM links
                  WHERE code = :code
                  LIMIT 1";
        $params = [
            'code' => $code,
        ];
        $data = $this->connection->fetchOne($query, $params);

        if (empty($data)) {
            throw new \Exception('empty');
        }
        $this->model->hydrate(array_merge($data, [
            'code' => $code,
        ]));

        return $this->model->toArray();
    }

    /**
     * Сохранить новые данные
     *
     * @param $url
     * @param $code
     * @return array
     */
    public function setData($url, $code): array
    {
        $data = [
            'link' => $url,
            'link' => $code,
        ];
        $columns = array_keys($data);
        $values = array();
        foreach ($columns as $column) {
            $values[] = ":$column";
        }
        $columns = implode(', ', $columns);
        $values = implode(', ', $values);
        $query = "INSERT IGNORE INTO links ({$columns}) VALUES ({$values})";
        $result = $this->connection->insert($query, $data);

        $this->model->hydrate(array_merge($data, [
            'id' => $result,
        ]));

        return $this->model->toArray();
    }

}