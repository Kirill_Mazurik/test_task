<?php

namespace model;

interface Common
{
    public function getId(): int ;

    public function setId($id);

    public function toArray(): array;
}