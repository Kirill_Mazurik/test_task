<?php

ini_set('display_errors', "on");

require_once __DIR__.'/../vendor/autoload.php';

$app = require __DIR__.'/../src/app.php';
require __DIR__.'/../config/prod.php';
require __DIR__.'/../src/controllers.php';
require_once __DIR__ . '/../config/routes.php';
require_once __DIR__ . '/../config/config.php';
$app->run();
